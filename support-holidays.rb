require "date" 
require "holidays"
require "gitlab"

# Make sure to set environmental variables:
# GITLAB_API_ENDPOINT = https://gitlab.com/api/v4 (or your own instance)
# GITLAB_API_PRIVATE_TOKEN = 'a private token generated on your instance'

gitlab_project_id = 7232931

client = Gitlab.client()

localities = [:ca,:us,:ru,:au,:my,:br,:ie,:gb]

from = Date.civil(Date.today.year,Date.today.month+1,1) #from the beginning of next month
to = Date.civil(from.year,from.month,-1) #until the end of next month

#issue_title = "Holiday Schedule for "+Date::MONTHNAMES[Date.today.month]+" "+String(Date.today.year)
issue_title = "Availability Schedule for: "+String(from)+" to "+String(to)

puts issue_title

holidays_hash = {}

#initialize the hash so we don't have to sort later.
(from.day..to.day).each do |n|
	holidays_hash[Date.civil(from.year,from.month,n)] = ""
end

issue_text = 
"| Date   |Holidays|Who's taking off?|\n"+
"|--------|--------|-----------------|\n"

for locality in localities
	holidays = Holidays.between(from, to, locality)
	for holiday in holidays
		
		#if there's an entry, add a new line
		if holidays_hash[holiday[:date]].length > 0
			holidays_hash[holiday[:date]] += "<br> " #may break when we move from redcarpet
		end

		#append holidays into appropriate array in hash
		holidays_hash[holiday[:date]] += holiday[:name]+" ("+String(locality).upcase+")"
	end
end
for key,val in holidays_hash
	puts key,val
	issue_text += "|" + String(key) + "|"+ val +"|      |\n"
end
puts issue_text
#create the issue
client.create_issue(gitlab_project_id,issue_title, {description: issue_text})
